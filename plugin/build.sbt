import sbt.Keys.publishTo

onLoad in Global := ((s: State) => {
  "updateIdea" :: s
}) compose (onLoad in Global).value

val projectName = "matt-idea-plugin"

val versionNumber = "1.1.86"

lazy val root: Project =
  Project(projectName, file("."))
    .enablePlugins(SbtIdeaPlugin)
    .settings(
      name := projectName,
      version := versionNumber,
      scalaVersion := "2.12.8",
      ideaPluginName in ThisBuild := projectName,
      assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
      ideaInternalPlugins := Seq(),
      //ideaExternalPlugins := Seq(IdeaPlugin.Id("scala-plugin", "org.intellij.scala", None)),
      ideaExternalPlugins := Seq(IdeaPlugin.Zip("scala-plugin", url("https://plugins.jetbrains.com/files/1347/57615/scala-intellij-bin-2018.3.6.zip?updateId=57615&pluginId=1347&family=intellij"))),
      ideaDownloadDirectory in ThisBuild := baseDirectory.value / "idea",
      aggregate in updateIdea := false,
      assemblyExcludedJars in assembly := ideaFullJars.value,
      ideaBuild := "183.5912.21", //IDEA 2018.3.5
    )
