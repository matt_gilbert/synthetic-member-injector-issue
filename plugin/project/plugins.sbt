logLevel := Level.Warn

addSbtPlugin("org.jetbrains" % "sbt-idea-plugin" % "2.2.6")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")
