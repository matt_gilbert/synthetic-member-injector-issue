package com.sa.plugin.idea.injector

import com.intellij.notification.{Notification, NotificationType, Notifications}
import org.jetbrains.plugins.scala.lang.psi.api.toplevel.typedef.{ScClass, ScObject, ScTypeDefinition}
import org.jetbrains.plugins.scala.lang.psi.impl.toplevel.typedef.SyntheticMembersInjector

import scala.util.control.NonFatal

class Injector extends SyntheticMembersInjector {

  private val AnnotationName = "com.sa.macros.ValidatedRequest"

  override def injectFunctions(source: ScTypeDefinition): Seq[String] =
    source match {
      case obj: ScObject => obj.fakeCompanionClassOrCompanionClass match {
        case clazz: ScClass if hasAnnotation(clazz, AnnotationName) => addCustomFunctions(obj)
        case _ => Seq.empty
      }
      case _ => Seq.empty
    }

  private def notify(title: String, message: String): Unit = {
    val notification = new Notification("group", title, message, NotificationType.ERROR)

    Notifications.Bus.notify(notification)
  }
  private def addCustomFunctions(obj: ScObject): Seq[String] = {
    try {
      obj.getMethods

      /*

      This above call to getMethods throws heaps of StackOverflowError's

      Like this:
      9:41 PM	Exception: com.intellij.openapi.progress.ProcessCanceledException: java.lang.StackOverflowError

      9:41 PM	Exception: com.intellij.openapi.progress.ProcessCanceledException: java.lang.StackOverflowError

      9:41 PM	Exception: com.intellij.openapi.progress.ProcessCanceledException: java.lang.StackOverflowError

      9:41 PM	Exception: com.intellij.openapi.progress.ProcessCanceledException: java.lang.StackOverflowError

      9:41 PM	Exception: com.intellij.openapi.progress.ProcessCanceledException: java.lang.StackOverflowError

       */
    } catch {
      case NonFatal(e) => notify("Exception", e.toString)
    }

    Nil
  }

  private def hasAnnotation(clazz: ScClass, annotationName: String): Boolean =
    Option(clazz.findAnnotation(annotationName)).isDefined

}
