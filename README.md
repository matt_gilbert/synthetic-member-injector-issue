## How to reproduce issue

### 1. Build & Install Plugin

```
cd plugin
sbt
> package
```
Get .jar out of target and load into idea via File -> Settings -> Plugins

### 2. See StackOverflowErrors

Open idea and go File -> New -> Project From Existing Sources

point to example/build.sbt

Open project in Idea and go to com.sa.macros.Usage file and observe StackOverflowErrors in console

