package com.sa.macros

import scala.language.experimental.macros
import scala.reflect.macros.blackbox

class ValidatedRequest() extends scala.annotation.StaticAnnotation {
  def macroTransform(annottees: Any*): Any = macro WithFieldsImpl.validatedRequestMacro
}

class WithFieldsImpl(val c: blackbox.Context) {

  def validatedRequestMacro(annottees: c.Expr[Any]*): c.Expr[Any] = {
    annottees.head
  }

}
