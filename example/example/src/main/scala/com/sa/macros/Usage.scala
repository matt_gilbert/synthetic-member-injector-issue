package com.sa.macros

object Usage {

  @ValidatedRequest
  case class TestRequest(name: Option[String], email: Option[String])

  object TestRequest {

    implicit val requireName = RequiredField[TestRequest, String]()
    implicit val requireEmail = RequiredField[TestRequest, String]()

  }

}
