
lazy val macroExample: Project =
  Project("macroExample", file("macroExample"))
    .settings(
      scalaVersion := "2.12.8",
      resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
      addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full),
      libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
    )

lazy val root: Project =
  Project("example", file("example"))
    .settings(
      scalaVersion := "2.12.8",
      resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
      addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)
    )
    .dependsOn(macroExample)
    .aggregate(macroExample)
